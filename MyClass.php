<?php
/**
 * Created by PhpStorm.
 * User: deurl
 * Date: 10.02.2019
 * Time: 17:48
 */

class MyClass
{
    public $someProp;
    
    public function someMethod($i)
    {
        return "Hello world" . ($i + 1);
    }
}